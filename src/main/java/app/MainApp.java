package app;

import org.hibernate.Session;

import modelo.Articulo;
import modelo.Cliente;
import modelo.Factura;
import modelo.Grupo;
import modelo.Linfactura;
import modelo.Vendedor;

public class MainApp {
	static Session sesion;

	public static void main(String[] args) {
//		@SuppressWarnings("unused")
		java.util.logging.Logger.getLogger("org.hibernate").setLevel(java.util.logging.Level.SEVERE);

		System.out.println("APP - EMPRESA CON HIBERNATE");
		System.out.println("---------------------------");

		try {
			sesion = ConexionHB.getSession();
			
			if (sesion != null) {
				Articulo a = sesion.get(Articulo.class, 1);
				System.out.println(a);
				Cliente c = sesion.get(Cliente.class, 1);
				System.out.println(c);
				Grupo g = sesion.get(Grupo.class, 1);
				System.out.println(g);
				Vendedor v = sesion.get(Vendedor.class, 1);
				System.out.println(v);
				Factura f = sesion.get(Factura.class, 1);
				System.out.println(f);
				for (Linfactura lf : f.getLinfacturas()) {
					System.out.println("\t" + lf);
				}

				Grupo g2 = sesion.get(Grupo.class, 2);
				Articulo artNuevo = new Articulo(g2, "artnuevo", 12f, "cod2", 10);
				sesion.getTransaction().begin();
				sesion.persist(artNuevo);
				sesion.getTransaction().commit();
				System.out.println("Insertado --> " + artNuevo);
				
				ConexionHB.closeSession();
			}

		} catch (Exception e) {
			System.err.println("Error en alguna operación del programa..." + e.getMessage());
		}

		System.out.println("-------------------------------");
		System.out.println("FIN APP - EMPRESA CON HIBERNATE");

	}

}
